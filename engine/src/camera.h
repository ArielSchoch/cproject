#pragma once
#include "utils/list.h"
#include "transform.h"
#include "linmath.h"

typedef struct CameraData
{
	Transform transform;
	float fov;
	float near_clip;
	float far_clip;
	int width;
	int height;
} CameraData;

void camera_projection_matrix(mat4x4 out, CameraData* camera_data);
void camera_view_matrix(mat4x4 out, CameraData* camera_data);
void camera_draw(CameraData* camera, List* opaque_renderers, List* transparent_renderers);
