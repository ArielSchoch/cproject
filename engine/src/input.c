#include "input.h"

GLFWwindow* window;

void input_init(GLFWwindow* w)
{
	window = w;
}

bool input_get_key_down(int key)
{
	return glfwGetKey(window, key) == GLFW_PRESS;
}

bool input_get_key_up(int key)
{
	return glfwGetKey(window, key) == GLFW_RELEASE;
}

int input_get_mouse_button(int button)
{
	return glfwGetMouseButton(window, button);
}

void input_get_cursor_position(double* pos_x, double* pos_y)
{
	glfwGetCursorPos(window, pos_x, pos_y);
}