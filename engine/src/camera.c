#include "camera.h"
#include "renderer.h"
#include "utils/utils.h"

void camera_projection_matrix(mat4x4 out, CameraData* camera_data)
{
	float aspect_ratio = camera_data->width / (float)camera_data->height;
	//mat4x4_ortho(out, -1.0f * aspect_ratio, 1.0f * aspect_ratio, -1.0f, 1.0f, 0.1f, 100.0f);
	mat4x4_perspective(out, camera_data->fov, aspect_ratio, camera_data->near_clip, camera_data->far_clip);
}

void camera_view_matrix(mat4x4 out, CameraData* camera_data)
{
	mat4x4 camera_mat;
	build_matrix(camera_mat, camera_data->transform.scale, camera_data->transform.rotation, camera_data->transform.rot, camera_data->transform.rotation_quat, camera_data->transform.position);
	mat4x4_invert(out, camera_mat);
}

void camera_draw(CameraData* camera, List* opaque_renderers, List* transparent_renderers)
{
	mat4x4 projection;
	camera_projection_matrix(projection, camera);

	mat4x4 view;
	camera_view_matrix(view, camera);

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	for (uint i = 0; i < opaque_renderers->count; i++)
		renderer_draw(list_get(opaque_renderers, i), view, projection);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glDepthMask(GL_FALSE);

	for (uint i = 0; i < transparent_renderers->count; i++)
		renderer_draw(list_get(transparent_renderers, i), view, projection);
}
