#pragma once
#define _GNU_SOURCE
#include <stdbool.h>

void engine_init(void (*start_func_ptr)(), void (*update_func_ptr)(double delta_time, double time_elapsed) /* keyCallback, targetFrameRate */);
void engine_set_fullscreen(bool fullscreen);
void engine_set_clear_color(float color[4]);
int engine_get_window_width();
int engine_get_window_height();
