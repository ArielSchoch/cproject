#pragma once
#include "linmath.h"

float lerp(float a, float b, float t);
float inverse_lerp(float a, float b, float v);
void build_matrix(mat4x4 out, vec3 scale, vec3 rotation_axis, float rotation, quat q, vec3 translation);
