#include "utils.h"

float lerp(float a, float b, float t)
{
    return a * (1 - t) + b * t;
}

float inverse_lerp(float a, float b, float v)
{
    return (v - a) / (b - a);
}

void build_matrix(mat4x4 out, vec3 scale, vec3 rotation_axis, float rotation, quat q, vec3 translation)
{
    mat4x4 translation_mat;
    mat4x4_identity(translation_mat);
    mat4x4_translate_in_place(translation_mat, translation[0], translation[1], translation[2]);

    mat4x4 quat_mat;
    mat4x4_from_quat(quat_mat, q);
    mat4x4 rotation_mat;
    mat4x4_mul(rotation_mat, translation_mat, quat_mat);
    //mat4x4_rotate(rotation_mat, translation_mat, rotation_axis[0], rotation_axis[1], rotation_axis[2], rotation);
    //mat4x4o_mul_quat(rotation_mat, translation_mat, q);

    mat4x4 scale_mat;
    mat4x4_scale_aniso(scale_mat, rotation_mat, scale[0], scale[1], scale[2]);
    mat4x4_dup(out, scale_mat);
}
