#pragma once
#include <stdlib.h>

typedef unsigned int uint;

typedef struct
{
    uint size;
    uint count;
    size_t item_size;
    char *items;
} List;

typedef char * (*item_to_string_func)(void *item);

void list_init(List *list, size_t item_size, uint size);
void *list_get(List *list, uint index);
void list_set(List *list, uint index, void *value);
void list_add(List *list, void *item);
void list_remove(List *list, int index);
void list_remove_item(List *list, void *item);
int list_get_index(List *list, void *item);
void list_print(List *list, item_to_string_func to_string);
void list_destroy(List list);