#include <stdio.h>
#include <stdbool.h>
#include <memory.h>
#include "list.h"

void list_init(List *list, size_t item_size, uint size)
{
    list->size = size;
    list->count = 0;
    list->item_size = item_size;
    list->items = calloc(size, item_size);
}

void *list_get(List *list, uint index)
{
    return list->items + index * list->item_size;
}

void list_set(List *list, uint index, void *value)
{
    if (index >= list->count)
    {
        printf("[ERROR] Index %i out of bounds of list with %i items! [%s:%d]\n", index, list->count, __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }

    memcpy(list->items + index * list->item_size, value, list->item_size);
}

void list_add(List *list, void *item)
{
    // Check if list size needs to grow
    if (list->size == list->count)
    {
        if (list->size == 0)
            list->size = 1;
        else
            list->size *= 2;

        char *items = realloc(list->items, list->size * list->item_size);
        if (list->items == NULL)
            printf("[ERROR] Couldn't reallocate memory for list of size %i! [%s:%d]\n", list->size, __FILE__, __LINE__);
        else
            list->items = items;
    }

    memcpy(list->items + list->count * list->item_size, item, list->item_size);
    list->count++;
}

void list_remove(List *list, int index)
{
    if (index >= list->count)
    {
        printf("[ERROR] Index %i out of bounds of list with %i items! [%s:%d]\n", index, list->count, __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }

    void *previous = calloc(1, list->item_size);
    void *current = calloc(1, list->item_size);
    for (int i = list->count - 1; i >= index; i--)
    {
        memcpy(previous, current, list->item_size); // previous = current;
        memcpy(current, list->items + i * list->item_size, list->item_size); // current = list->items[i];
        memcpy(list->items + i * list->item_size, previous, list->item_size); // list->items[i] = previous;
    }
    list->count--;
    free(previous);
    free(current);
}

void list_remove_item(List *list, void *item)
{
    int start_index = list_get_index(list, item);
    if (start_index >= 0)
    {
        int tail_count = list->count - start_index;
        if (tail_count > 0)
        {
            void *dst = list->items + start_index * list->item_size;
            void *src = list->items + (start_index + 1) * list->item_size;
            if (memmove(dst, src, list->item_size * tail_count) == NULL)
            {
                printf("[ERROR] Failed to move memory while removing item from list! [%s:%d]\n", __FILE__, __LINE__);
                exit(EXIT_FAILURE);
            }
        }

        list->count--;
    }
    else
    {
        printf("[WARNING] Tried to remove non existent item %p from list!", item);
    }
}

int list_get_index(List *list, void *item)
{
    for (int i = list->count - 1; i >= 0; i--)
        if (memcmp(list_get(list, i), item, list->item_size) == 0)
            return i;
    return -1;
}

void list_print(List *list, item_to_string_func to_string)
{
    printf("List { capacity: %i, items: %i }\n", list->size, list->count);
    for (uint i = 0; i < list->count; i++)
        printf("[%i]: %s\n", i, to_string(list_get(list, i)));
}

void list_destroy(List list)
{
    free(list.items);
}