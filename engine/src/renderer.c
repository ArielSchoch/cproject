#include "renderer.h"
#include "utils/utils.h"

void renderer_draw(RendererData* renderer, mat4x4 view, mat4x4 projection)
{
	Transform* transform = renderer->transform;
	mat4x4 model;
	build_matrix(model, transform->scale, transform->rotation, transform->rot, transform->rotation_quat, transform->position);
	mesh_draw(renderer->mesh, renderer->shader, model, view, projection);
}
