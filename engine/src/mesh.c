#include "mesh.h"

void mesh_init(Mesh* mesh, int vertex_count, unsigned int vertex_size, float* vertices)
{
    mesh->vertex_count = vertex_count;
    mesh->vertices = vertices;
    // Create buffers
    glGenVertexArrays(1, &mesh->vao);
    glGenBuffers(1, &mesh->vbo);

    // Select which buffers to modify
    glBindVertexArray(mesh->vao);
    glBindBuffer(GL_ARRAY_BUFFER, mesh->vbo);

    // Fill our vertex buffer object with data
    glBufferData(GL_ARRAY_BUFFER, mesh->vertex_count * vertex_size, mesh->vertices, GL_STATIC_DRAW);

    // Specify our interleaved vertex array format
    // First arguement is the attribute index which will be used for the layout parameter in our shader
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, vertex_size, (void*)(0 * sizeof(float)));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, vertex_size, (void*)(3 * sizeof(float)));
    
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    // Done with our buffers
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void mesh_draw(Mesh* mesh, Shader* shader, mat4x4 model, mat4x4 view, mat4x4 projection)
{
    shader_use(shader);
    shader_set_mat4_direct(shader, "model", model);
    shader_set_mat4_direct(shader, "view", view);
    shader_set_mat4_direct(shader, "proj", projection);

    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);
    //glFrontFace(GL_CW);
    glBindVertexArray(mesh->vao);
    glDrawArrays(GL_TRIANGLES, 0, mesh->vertex_count);
    glBindVertexArray(0);
    glUseProgram(0);
}

void mesh_primitive_quad(Mesh* mesh)
{
    float vertices[] = {
        -0.5, 0.5, 0.0, 1.0, 0.0, 0.0,   // top left
        0.5, 0.5, 0.0, 0.0, 1.0, 0.0,    // top right
        -0.5, -0.5, 0.0, 0.0, 0.0, 1.0,  // bottom left

        0.5, 0.5, 0.0, 0.0, 1.0, 0.0,    // top right
        0.5, -0.5, 0.0, 0.0, 1.0, 1.0,   // bottom right
        -0.5, -0.5, 0.0, 0.0, 0.0, 1.0,  // bottom left
    };
    float* verts = (float *) malloc(sizeof(vertices));
    memcpy(verts, vertices, sizeof(vertices));

    mesh_init(mesh, 6, 6 * sizeof(float), verts);
}

void mesh_primitive_cube(Mesh* mesh)
{
    float vertices[] = {
        -1.0f,-1.0f,-1.0f, 1.0f,0.0f,0.0f,
        -1.0f,-1.0f, 1.0f, 1.0f,0.0f,0.0f,
        -1.0f, 1.0f, 1.0f, 1.0f,0.0f,0.0f,
        1.0f, 1.0f,-1.0f, 0.0f,1.0f,0.0f,
        -1.0f,-1.0f,-1.0f, 0.0f,1.0f,0.0f,
        -1.0f, 1.0f,-1.0f, 0.0f,1.0f,0.0f,
        1.0f,-1.0f, 1.0f, 0.0f,0.0f,1.0f,
        -1.0f,-1.0f,-1.0f, 0.0f,0.0f,1.0f,
        1.0f,-1.0f,-1.0f, 0.0f,0.0f,1.0f,
        1.0f, 1.0f,-1.0f, 0.5f,0.5f,0.0f,
        1.0f,-1.0f,-1.0f, 0.5f,0.5f,0.0f,
        -1.0f,-1.0f,-1.0f, 0.5f,0.5f,0.0f,
        -1.0f,-1.0f,-1.0f, 0.0f,0.5f,0.5f,
        -1.0f, 1.0f, 1.0f, 0.0f,0.5f,0.5f,
        -1.0f, 1.0f,-1.0f, 0.0f,0.5f,0.5f,
        1.0f,-1.0f, 1.0f, 0.5f,0.0f,0.5f,
        -1.0f,-1.0f, 1.0f, 0.5f,0.0f,0.5f,
        -1.0f,-1.0f,-1.0f, 0.5f,0.0f,0.5f,
        -1.0f, 1.0f, 1.0f, 1.0f,0.0f,0.0f,
        -1.0f,-1.0f, 1.0f, 1.0f,0.0f,0.0f,
        1.0f,-1.0f, 1.0f, 1.0f,0.0f,0.0f,
        1.0f, 1.0f, 1.0f, 0.0f,1.0f,0.0f,
        1.0f,-1.0f,-1.0f, 0.0f,1.0f,0.0f,
        1.0f, 1.0f,-1.0f, 0.0f,1.0f,0.0f,
        1.0f,-1.0f,-1.0f, 0.0f,0.0f,1.0f,
        1.0f, 1.0f, 1.0f, 0.0f,0.0f,1.0f,
        1.0f,-1.0f, 1.0f, 0.0f,0.0f,1.0f,
        1.0f, 1.0f, 1.0f, 0.5f,0.5f,0.0f,
        1.0f, 1.0f,-1.0f, 0.5f,0.5f,0.0f,
        -1.0f, 1.0f,-1.0f, 0.5f,0.5f,0.0f,
        1.0f, 1.0f, 1.0f, 0.0f,0.5f,0.5f,
        -1.0f, 1.0f,-1.0f, 0.0f,0.5f,0.5f,
        -1.0f, 1.0f, 1.0f, 0.0f,0.5f,0.5f,
        1.0f, 1.0f, 1.0f, 0.5f,0.0f,0.5f,
        -1.0f, 1.0f, 1.0f, 0.5f,0.0f,0.5f,
        1.0f,-1.0f, 1.0f, 0.5f,0.0f,0.5f,
    };
    float* verts = (float*)malloc(sizeof(vertices));
    memcpy(verts, vertices, sizeof(vertices));

    size_t vertex_size = 6 * sizeof(float);
    mesh_init(mesh, sizeof(vertices) / vertex_size, vertex_size, verts);
}