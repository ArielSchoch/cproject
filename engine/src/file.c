#include "file.h"

char* file_read(char* filename)
{
    char* buffer = 0;
    long length;
    FILE* file;
    fopen_s(&file, filename, "rb");

    if (file)
    {
        fseek(file, 0, SEEK_END);
        length = ftell(file);
        fseek(file, 0, SEEK_SET);
        buffer = (char*)malloc(length + 1);

        if (buffer)
        {
            fread(buffer, 1, length, file);
            buffer[length] = '\0';
        }

        fclose(file);
    }

    if (!buffer)
    {
        printf("Failed to open file %s\n", filename);
        exit(EXIT_FAILURE);
    }

    return buffer;
}
