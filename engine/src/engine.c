#include "engine.h"
#include "input.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

void (*update_callback)(double, double);

GLFWwindow* window;
int width, height;
float clear_color[] = { 0.0f, 0.0f, 0.0f, 0.0f };

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        glfwDestroyWindow(window);
        glfwTerminate();
        exit(EXIT_SUCCESS);
    }
}

void engine_set_fullscreen(bool fullscreen)
{
    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(monitor);
    glfwSetWindowMonitor(window, fullscreen ? monitor : NULL, 0, 0, mode->width, mode->height, mode->refreshRate);
}

void engine_set_clear_color(float color[4])
{
    for (int i = 0; i < 4; i++)
        clear_color[i] = color[i];
}

int engine_get_window_width() { return width; }

int engine_get_window_height() { return height; }

void window_size_callback(GLFWwindow* window, int w, int h)
{
    // TODO Rerender scene
    width = w;
    height = h;
    glViewport(0, 0, w, h);
    glClear(GL_COLOR_BUFFER_BIT);
    //glfwSwapBuffers(window);
}

void engine_update(GLFWwindow* window, double delta_time, double time_elapsed)
{
    char fps[50];
    snprintf(fps, 50, "CEngine %.0f", 1.0 / delta_time);
    glfwSetWindowTitle(window, fps);

    (*update_callback)(delta_time, time_elapsed);
}

GLFWwindow* create_window()
{
    if (!glfwInit())
    {
        printf("GLFW failed to be initialized. Exiting.\n");
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    const GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(monitor);

    glfwWindowHint(GLFW_RED_BITS, mode->redBits);
    glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
    glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
    glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
    
    GLFWwindow* window = glfwCreateWindow(mode->width, mode->height, "CEngine", NULL, NULL);
    if (!window)
    {
        printf("Failed to create a new window. Exiting.\n");
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwSetKeyCallback(window, key_callback);
    glfwSetWindowSizeCallback(window, window_size_callback);

    glfwMakeContextCurrent(window);

    if (glewInit() != GLEW_OK)
    {
        printf("GLEW failed to be initialized. Exiting.\n");
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    
    glfwSwapInterval(0); // V-Sync

    return window;
}

void engine_init(void (*start_func_ptr)(), void (*update_func_ptr)(double delta_time, double time_elapsed))
{
    double time_engine_start = glfwGetTime();
    update_callback = update_func_ptr;
    window = create_window();
    glfwGetFramebufferSize(window, &width, &height);
    
    input_init(window);
    start_func_ptr();

    double frame_duration = 0.0001;
    while (!glfwWindowShouldClose(window))
    {
        double time_frame_start = glfwGetTime();
        double time_elapsed = time_engine_start - time_frame_start;
        glfwGetFramebufferSize(window, &width, &height);
        glViewport(0, 0, width, height);

        glClearColor(clear_color[0], clear_color[1], clear_color[2], clear_color[3]);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);
        engine_update(window, frame_duration, time_elapsed);

        glfwSwapBuffers(window);
        glfwPollEvents();

        double time_frame_end = glfwGetTime();
        frame_duration = time_frame_end - time_frame_start;
    }

    glfwDestroyWindow(window);
    glfwTerminate();
}