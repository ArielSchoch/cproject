#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <GL/glew.h>
#include "linmath.h"
#include "utils/list.h"

typedef struct Shader
{
	GLuint program_id;
	char* vertex_source;
	char* fragment_source;
	List uniforms_int;
	List uniforms_float;
	List uniforms_vec2;
	List uniforms_vec3;
	List uniforms_vec4;
	List uniforms_mat4;
} Shader;

typedef struct UniformVariable
{
	char* uniform_name;
	void* value;
} UniformVariable;

void shader_load(Shader* shader);
void shader_use(Shader* shader);
void shader_set_bool(Shader* shader, char* name, bool value);
void shader_set_int(Shader* shader, char* name, int value);
void shader_set_float(Shader* shader, char* name, float value);
void shader_set_vec2(Shader* shader, char* name, vec2 value);
void shader_set_vec3(Shader* shader, char* name, vec3 value);
void shader_set_vec4(Shader* shader, char* name, vec4 value);
void shader_set_mat4(Shader* shader, char* name, mat4x4 value);
void shader_set_mat4_direct(Shader* shader, char* name, mat4x4 value);
