#include "shader.h"

void shader_load(Shader* shader)
{
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &shader->vertex_source, NULL);
	glCompileShader(vs);
	GLint vs_compile_success;
	glGetShaderiv(vs, GL_COMPILE_STATUS, &vs_compile_success);
	if (vs_compile_success == GL_FALSE)
	{
		printf("Vertex shader compilation failed.\n");
		GLint maxLength = 0;
		glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &maxLength);
		char* error = malloc(sizeof(char) * maxLength);
		glGetShaderInfoLog(vs, maxLength, &maxLength, error);
		printf("%s", error);
		exit(EXIT_FAILURE);
	}

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &shader->fragment_source, NULL);
	glCompileShader(fs);
	GLint fs_compile_success;
	glGetShaderiv(fs, GL_COMPILE_STATUS, &fs_compile_success);
	if (fs_compile_success == GL_FALSE)
	{
		printf("Fragment shader compilation failed.\n");
		GLint maxLength = 0;
		glGetShaderiv(fs, GL_INFO_LOG_LENGTH, &maxLength);
		char* error = malloc(sizeof(char) * maxLength);
		glGetShaderInfoLog(fs, maxLength, &maxLength, error);
		printf("%s", error);
		exit(EXIT_FAILURE);
	}

	shader->program_id = glCreateProgram();
	glAttachShader(shader->program_id, vs);
	glAttachShader(shader->program_id, fs);
	glLinkProgram(shader->program_id);

	glDetachShader(shader->program_id, vs);
	glDetachShader(shader->program_id, fs);
	glDeleteShader(vs);
	glDeleteShader(fs);

	list_init(&shader->uniforms_int, sizeof(UniformVariable), 0);
	list_init(&shader->uniforms_float, sizeof(UniformVariable), 0);
	list_init(&shader->uniforms_vec2, sizeof(UniformVariable), 0);
	list_init(&shader->uniforms_vec3, sizeof(UniformVariable), 0);
	list_init(&shader->uniforms_vec4, sizeof(UniformVariable), 0);
	list_init(&shader->uniforms_mat4, sizeof(UniformVariable), 0);
}

void shader_use(Shader* shader)
{
	glUseProgram(shader->program_id);

	for (uint i = 0; i < shader->uniforms_int.count; i++)
	{
		UniformVariable* uniform = list_get(&shader->uniforms_int, i);
		GLint loc = glGetUniformLocation(shader->program_id, uniform->uniform_name);
		glUniform1i(loc, *((int*) uniform->value));
	}

	for (uint i = 0; i < shader->uniforms_float.count; i++)
	{
		UniformVariable* uniform = list_get(&shader->uniforms_float, i);
		GLint loc = glGetUniformLocation(shader->program_id, uniform->uniform_name);
		glUniform1f(loc, *((float*) uniform->value));
	}

	for (uint i = 0; i < shader->uniforms_vec2.count; i++)
	{
		UniformVariable* uniform = list_get(&shader->uniforms_vec2, i);
		GLint loc = glGetUniformLocation(shader->program_id, uniform->uniform_name);
		glUniform2fv(loc, 1, uniform->value);
	}

	for (uint i = 0; i < shader->uniforms_vec3.count; i++)
	{
		UniformVariable* uniform = list_get(&shader->uniforms_vec3, i);
		GLint loc = glGetUniformLocation(shader->program_id, uniform->uniform_name);
		glUniform3fv(loc, 1, uniform->value);
	}

	for (uint i = 0; i < shader->uniforms_vec4.count; i++)
	{
		UniformVariable* uniform = list_get(&shader->uniforms_vec4, i);
		GLint loc = glGetUniformLocation(shader->program_id, uniform->uniform_name);
		glUniform4fv(loc, 1, uniform->value);
	}

	for (uint i = 0; i < shader->uniforms_mat4.count; i++)
	{
		UniformVariable* uniform = list_get(&shader->uniforms_mat4, i);
		GLint loc = glGetUniformLocation(shader->program_id, uniform->uniform_name);
		glUniformMatrix4fv(loc, 1, GL_FALSE, uniform->value);
	}
}

void uniform_list_replace(List* list, char* name, void* value)
{
	for (uint i = 0; i < list->count; i++)
	{
		UniformVariable* v = list_get(list, i);
		if (v->uniform_name == name)
		{
			list_remove(list, i);
			break;
		}
	}
	UniformVariable uniform = { name, value };
	list_add(list, &uniform);
}

void shader_set_bool(Shader* shader, char* name, bool value)
{
	uniform_list_replace(&shader->uniforms_int, name, &value);
}

void shader_set_int(Shader* shader, char* name, int value)
{
	uniform_list_replace(&shader->uniforms_int, name, &value);
}

void shader_set_float(Shader* shader, char* name, float value)
{
	uniform_list_replace(&shader->uniforms_float, name, &value);
}

void shader_set_vec2(Shader* shader, char* name, vec2 value)
{
	uniform_list_replace(&shader->uniforms_vec2, name, value);
}

void shader_set_vec3(Shader* shader, char* name, vec3 value)
{
	uniform_list_replace(&shader->uniforms_vec3, name, value);
}

void shader_set_vec4(Shader* shader, char* name, vec4 value)
{
	uniform_list_replace(&shader->uniforms_vec4, name, value);
}

void shader_set_mat4(Shader* shader, char* name, mat4x4 value)
{
	uniform_list_replace(&shader->uniforms_mat4, name, value);
}

void shader_set_mat4_direct(Shader* shader, char* name, mat4x4 value)
{
	glUniformMatrix4fv(glGetUniformLocation(shader->program_id, name), 1, GL_FALSE, *value);
}