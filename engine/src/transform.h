#pragma once
#include "linmath.h"

typedef struct Transform
{
	quat rotation_quat;
	vec3 position;
	vec3 rotation;
	vec3 scale;
	float rot;
	struct Transform* parent;
} Transform;
