#pragma once
#include <stdbool.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define KEY_ESCAPE		GLFW_KEY_ESCAPE
#define KEY_ENTER		GLFW_KEY_ENTER
#define KEY_TAB			GLFW_KEY_TAB
#define KEY_BACKSPACE	GLFW_KEY_BACKSPACE
#define KEY_INSERT		GLFW_KEY_INSERT
#define KEY_DELETE		GLFW_KEY_DELETE
#define KEY_RIGHT		GLFW_KEY_RIGHT
#define KEY_LEFT		GLFW_KEY_LEFT
#define KEY_DOWN		GLFW_KEY_DOWN
#define KEY_UP			GLFW_KEY_UP
#define KEY_A			GLFW_KEY_A
#define KEY_B			GLFW_KEY_B
#define KEY_C			GLFW_KEY_C
#define KEY_D			GLFW_KEY_D
#define KEY_E			GLFW_KEY_E
#define KEY_F			GLFW_KEY_F
#define KEY_G			GLFW_KEY_G
#define KEY_H			GLFW_KEY_H
#define KEY_I			GLFW_KEY_I
#define KEY_J			GLFW_KEY_J
#define KEY_K			GLFW_KEY_K
#define KEY_L			GLFW_KEY_L
#define KEY_M			GLFW_KEY_M
#define KEY_N			GLFW_KEY_N
#define KEY_O			GLFW_KEY_O
#define KEY_P			GLFW_KEY_P
#define KEY_Q			GLFW_KEY_Q
#define KEY_R			GLFW_KEY_R
#define KEY_S			GLFW_KEY_S
#define KEY_T			GLFW_KEY_T
#define KEY_U			GLFW_KEY_U
#define KEY_V			GLFW_KEY_V
#define KEY_W			GLFW_KEY_W
#define KEY_X			GLFW_KEY_X
#define KEY_Y			GLFW_KEY_Y
#define KEY_Z			GLFW_KEY_Z

#define MOUSE_BUTTON_1		GLFW_MOUSE_BUTTON_1
#define MOUSE_BUTTON_2		GLFW_MOUSE_BUTTON_2
#define MOUSE_BUTTON_3		GLFW_MOUSE_BUTTON_3
#define MOUSE_BUTTON_4		GLFW_MOUSE_BUTTON_4
#define MOUSE_BUTTON_5		GLFW_MOUSE_BUTTON_5
#define MOUSE_BUTTON_6		GLFW_MOUSE_BUTTON_6
#define MOUSE_BUTTON_7		GLFW_MOUSE_BUTTON_7
#define MOUSE_BUTTON_8		GLFW_MOUSE_BUTTON_8
#define MOUSE_BUTTON_LAST	GLFW_MOUSE_BUTTON_LAST
#define MOUSE_BUTTON_LEFT	GLFW_MOUSE_BUTTON_LEFT
#define MOUSE_BUTTON_RIGHT	GLFW_MOUSE_BUTTON_RIGHT
#define MOUSE_BUTTON_MIDDLE	GLFW_MOUSE_BUTTON_MIDDLE

#define INPUT_RELEASE GLFW_RELEASE
#define INPUT_PRESS GLFW_PRESS
#define INPUT_REPEAT GLFW_REPEAT

void input_init(GLFWwindow* w);
bool input_get_key_down(int key);
bool input_get_key_up(int key);
int input_get_mouse_button(int button);
void input_get_cursor_position(double* pos_x, double* pos_y);
