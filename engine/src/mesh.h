#pragma once
#include "shader.h"
#include <GL/glew.h>

typedef struct Mesh
{
	GLuint vbo;
	GLuint vao;
    int vertex_count;
	float* vertices;
} Mesh;

void mesh_init(Mesh* mesh, int vertex_count, unsigned int vertex_size, float* vertices);
void mesh_draw(Mesh* mesh, Shader* shader, mat4x4 model, mat4x4 view, mat4x4 projection);
void mesh_primitive_quad(Mesh* mesh);
void mesh_primitive_cube(Mesh* mesh);
