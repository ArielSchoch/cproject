#pragma once
#include "transform.h"
#include "mesh.h"
#include "shader.h"
#include "linmath.h"

typedef struct RendererData
{
	Transform* transform;
	Mesh* mesh;
	Shader* shader;
} RendererData;

void renderer_draw(RendererData* renderer, mat4x4 view, mat4x4 projection);
