#version 330 core

out vec4 FragColor;
in vec4 vertexColor;
in vec2 vertexPos;

void main()
{
    if (length(vertexPos) < 0.5)
        FragColor = vec4(vertexColor.xy, 0.8, 1.0);
    else
        FragColor = vec4(0.0, 0.0, 0.0, 0.0);
}