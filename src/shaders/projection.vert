#version 330 core

layout (location = 0) in vec3 position;
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
out vec4 VertPos;

void main()
{
	VertPos = proj * view * model * vec4(position.xyz, 1.0);
	gl_Position = VertPos;
}