#version 330 core

uniform vec4 color;
in vec4 VertPos;
out vec4 FragColor;

void main()
{
    FragColor = color * mix(1.0, 0.1, clamp(VertPos.z * 0.05, 0.0, 1.0));
}