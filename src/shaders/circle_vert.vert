#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
out vec4 vertexColor;
out vec2 vertexPos;

void main()
{
	vertexColor = vec4(color.rgb, 1.0);
	vertexPos = position.xy;
	gl_Position = proj * view * model * vec4(position.xyz, 1.0);
}