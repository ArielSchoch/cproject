#pragma once

#include "../engine/src/shader.h"

struct QuadColorData
{
    Shader* shader;
    float* color;
};
