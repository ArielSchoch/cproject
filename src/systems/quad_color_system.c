#include "quad_color_system.h"
#include "../engine/src/utils/utils.h"

void quad_color_system_init(struct QuadColorData* data)
{
    data->color = malloc(sizeof(float) * 4);
}

void quad_color_system_update(struct QuadColorData* data, double time_elapsed)
{
    data->color[0] = lerp(0.2f, 0.8f, inverse_lerp(-1.0f, 1.0f, (float) sin(time_elapsed)));
    data->color[1] = 0.6f;
    data->color[2] = 1.0f;
    data->color[3] = 1.0f;
    shader_set_vec4(data->shader, "color", data->color);
}