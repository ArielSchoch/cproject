#include "../engine/src/engine.h"
#include "../engine/src/input.h"
#include "../engine/src/camera.h"
#include "../engine/src/mesh.h"
#include "../engine/src/transform.h"
#include "../engine/src/Renderer.h"
#include "../engine/src/file.h"
#include "../engine/src/utils/utils.h"
#include "../engine/src/utils/list.h"
#include "linmath.h"

#include "../src/systems/quad_color_system.h"

#define PI 3.14159265358979323846

Shader basic_shader;
Shader circle_shader;
Shader color_shader;
Mesh quad_mesh;
Mesh cube_mesh;

RendererData quads[4];

vec3 scale = { 1.0f, 1.0f, 1.0f };
vec3 axis = { 0.0f, 1.0f, 0.0f };
float rotation = 0.0f;
vec3 pos = { -0.6f, -0.1f, 0.0f };

vec3 pos1 = { -0.6f, -0.1f, 0.0f };

float* pixel_buffer;

double prev_mouse_pos[2];
List opaque_renderers;
List transparent_renderers;
RendererData r1;
RendererData r2;
Transform camera_transform;
Transform t1;
Transform t2;

void start();
void update(double delta_time, double time_elapsed);
void features_init();

int main(int argc, char* argv[])
{
    engine_init(&start, &update);
    return 0;
}

void load_content()
{
    basic_shader.vertex_source = file_read(".\\src\\shaders\\basic_vert.vert");
    basic_shader.fragment_source = file_read(".\\src\\shaders\\basic_frag.frag");
    shader_load(&basic_shader);

    circle_shader.vertex_source = file_read(".\\src\\shaders\\circle_vert.vert");
    circle_shader.fragment_source = file_read(".\\src\\shaders\\circle_frag.frag");
    shader_load(&circle_shader);

    color_shader.vertex_source = file_read(".\\src\\shaders\\projection.vert");
    color_shader.fragment_source = file_read(".\\src\\shaders\\uniform_color.frag");
    shader_load(&color_shader);
}

void start()
{
    float clear_color[] = { 0.12f, 0.18f, 0.25f, 0.0f };
    engine_set_clear_color(clear_color);
    load_content();
    mesh_primitive_quad(&quad_mesh);
    mesh_primitive_cube(&cube_mesh);

    quads[0].mesh = malloc(sizeof(Mesh));
    mesh_primitive_quad(quads[0].mesh);
    quads[1].mesh = malloc(sizeof(Mesh));
    mesh_primitive_quad(quads[1].mesh);
    quads[2].mesh = malloc(sizeof(Mesh));
    mesh_primitive_quad(quads[2].mesh);
    quads[3].mesh = malloc(sizeof(Mesh));
    mesh_primitive_quad(quads[3].mesh);
    
    int buffer_width = 1920;
    int buffer_height = 1080;
    pixel_buffer = (float*)malloc(sizeof(float) * buffer_width * buffer_height * 3);
    for (int y = 0; y < buffer_height; y++)
    {
        for (int x = 0; x < buffer_width; x++)
        {
            int pixelLocation = (y * buffer_width * 3) + (x * 3);
            bool checkered = ((y / 120) + (x / 120)) % 2;
            pixel_buffer[pixelLocation + 0] = checkered ? 1.0 : 0.5;
            pixel_buffer[pixelLocation + 1] = checkered ? 1.0 : 0.5;
            pixel_buffer[pixelLocation + 2] = checkered ? 1.0 : 0.5;
            pixel_buffer[pixelLocation + 0] *= x / (float)buffer_width;
            pixel_buffer[pixelLocation + 1] *= y / (float)buffer_height;
        }
    }

    list_init(&opaque_renderers, sizeof(RendererData), 1);
    list_init(&transparent_renderers, sizeof(RendererData), 1);

    quat_identity(t1.rotation_quat);
    r1.transform = &t1;
    r1.mesh = &quad_mesh;
    r1.shader = &circle_shader;
    list_add(&transparent_renderers, &r1);

    quat_identity(t2.rotation_quat);
    r2.transform = &t2;
    r2.mesh = &cube_mesh;
    r2.shader = &basic_shader;
    list_add(&opaque_renderers, &r2);


    RendererData *quad_floor = &quads[0];
    quad_floor->shader = &color_shader;
    quad_floor->transform = calloc(1, sizeof(Transform));
    vec3 floor_rot = { PI * 0.5f, 0.0f, 0.0f };
    quat_euler(&quad_floor->transform->rotation_quat, floor_rot);
    quad_floor->transform->scale[0] = 6.0f;
    quad_floor->transform->scale[1] = 10.0f;
    quad_floor->transform->scale[2] = 1.0f;
    quad_floor->transform->position[1] = -3.0f;
    quad_floor->transform->position[2] = -4.0f;
    list_add(&opaque_renderers, quad_floor);

    RendererData* quad_roof = &quads[1];
    quad_roof->shader = &color_shader;
    quad_roof->transform = calloc(1, sizeof(Transform));
    vec3 roof_rot = { PI * 0.5f, 0.0f, 0.0f };
    quat_euler(&quad_roof->transform->rotation_quat, roof_rot);
    quad_roof->transform->scale[0] = 6.0f;
    quad_roof->transform->scale[1] = 10.0f;
    quad_roof->transform->scale[2] = 1.0f;
    quad_roof->transform->position[1] = 3.0f;
    quad_roof->transform->position[2] = -4.0f;
    list_add(&opaque_renderers, quad_roof);

    RendererData* quad_left = &quads[2];
    quad_left->shader = &color_shader;
    quad_left->transform = calloc(1, sizeof(Transform));
    vec3 left_rot = { 0.0f, PI * 0.5f, 0.0f };
    quat_euler(&quad_left->transform->rotation_quat, left_rot);
    quad_left->transform->scale[0] = 10.0f;
    quad_left->transform->scale[1] = 6.0f;
    quad_left->transform->scale[2] = 1.0f;
    quad_left->transform->position[0] = -3.0f;
    quad_left->transform->position[1] = 0.0f;
    quad_left->transform->position[2] = -4.0f;
    list_add(&opaque_renderers, quad_left);

    RendererData* quad_right = &quads[3];
    quad_right->shader = &color_shader;
    quad_right->transform = calloc(1, sizeof(Transform));
    vec3 right_rot = { 0.0f, PI * 0.5f, 0.0f };
    quat_euler(&quad_right->transform->rotation_quat, right_rot);
    quad_right->transform->scale[0] = 10.0f;
    quad_right->transform->scale[1] = 6.0f;
    quad_right->transform->scale[2] = 1.0f;
    quad_right->transform->position[0] = 3.0f;
    quad_right->transform->position[1] = 0.0f;
    quad_right->transform->position[2] = -4.0f;
    list_add(&opaque_renderers, quad_right);


    quat_identity(camera_transform.rotation_quat);
    camera_transform.scale[0] = 1.0f;
    camera_transform.scale[1] = 1.0f;
    camera_transform.scale[2] = 1.0f;
    camera_transform.position[2] = 5.0f;
    input_get_cursor_position(prev_mouse_pos, prev_mouse_pos + 1);

    features_init();
}

void fullscreen_system()
{
    if (input_get_key_down(KEY_F))
        engine_set_fullscreen(true);
    else if (input_get_key_down(KEY_M))
        engine_set_fullscreen(false);
}

void movement_system(double delta_time, double time_elapsed)
{
    float move_speed = 4.0;
    if (input_get_key_down(KEY_UP))
        pos[1] += move_speed * delta_time;
    if (input_get_key_down(KEY_DOWN))
        pos[1] -= move_speed * delta_time;
    if (input_get_key_down(KEY_RIGHT))
        pos[0] += move_speed * delta_time;
    if (input_get_key_down(KEY_LEFT))
        pos[0] -= move_speed * delta_time;

    float camera_move_speed = 8.0;
    vec3 forward = { 0.0f, 0.0f, -1.0f };
    vec3 forward_dir;
    quat_mul_vec3(forward_dir, camera_transform.rotation_quat, forward);
    vec3 right = { 1.0f, 0.0f, 0.0f };
    vec3 right_dir;
    quat_mul_vec3(right_dir, camera_transform.rotation_quat, right);

    if (input_get_key_down(KEY_W))
    {
        vec3 movement;
        vec3_scale(movement, forward_dir, camera_move_speed * delta_time);
        vec3_add(camera_transform.position, camera_transform.position, movement);
        //camera_transform.position[1] += camera_move_speed * delta_time;
    }
    if (input_get_key_down(KEY_S))
    {
        vec3 movement;
        vec3_scale(movement, forward_dir, -camera_move_speed * delta_time);
        vec3_add(camera_transform.position, camera_transform.position, movement);
        //camera_transform.position[1] -= camera_move_speed * delta_time;
    }
    if (input_get_key_down(KEY_D))
    {
        vec3 movement;
        vec3_scale(movement, right_dir, camera_move_speed * delta_time);
        vec3_add(camera_transform.position, camera_transform.position, movement);
        //camera_transform.position[0] += camera_move_speed * delta_time;
    }
    if (input_get_key_down(KEY_A))
    {
        vec3 movement;
        vec3_scale(movement, right_dir, -camera_move_speed * delta_time);
        vec3_add(camera_transform.position, camera_transform.position, movement);
        //camera_transform.position[0] -= camera_move_speed * delta_time;
    }

    if (input_get_mouse_button(MOUSE_BUTTON_RIGHT) == INPUT_PRESS)
    {
        double mouse_pos[2];
        input_get_cursor_position(mouse_pos, mouse_pos + 1);
        double delta_mouse_pos[] = { mouse_pos[0] - prev_mouse_pos[0], mouse_pos[1] - prev_mouse_pos[1] };
        float mouse_speed = 0.003f;

        vec3 euler_angles = { -delta_mouse_pos[1] * mouse_speed, -delta_mouse_pos[0] * mouse_speed, 0.0f };
        vec3 up = { 0.0f, 1.0f, 0.0f };
        vec3 up_dir;
        quat_mul_vec3(up_dir, camera_transform.rotation_quat, up);
        quat horizontal_rot;
        quat_rotate(horizontal_rot, euler_angles[1], up);
        //horizontal_rot[2] = 0.0f;

        vec3 right = { 1.0f, 0.0f, 0.0f };
        vec3 right_dir;
        quat_mul_vec3(right_dir, camera_transform.rotation_quat, right);
        quat vertical_rot;
        quat_rotate(vertical_rot, euler_angles[0], right_dir);
        //quat_euler(vertical_rot, euler_angles);
        vertical_rot[2] = 0.0f;
        quat combined_rot;
        quat_mul(combined_rot, horizontal_rot, vertical_rot);
        combined_rot[2] = 0.0f;
        quat final_rot;
        quat_mul(final_rot, camera_transform.rotation_quat, combined_rot);
        final_rot[2] = 0.0f;
        vec4_dup(camera_transform.rotation_quat, final_rot);
        printf("quat: [%.3f %.3f %.3f %.3f]\n", vertical_rot[0], vertical_rot[1], vertical_rot[2], vertical_rot[3]);

        //quat mouse_rot;
        //vec3 euler_angles = { -delta_mouse_pos[1] * mouse_speed, -delta_mouse_pos[0] * mouse_speed, 0.0f };
        //quat_euler(mouse_rot, euler_angles);
        //mouse_rot[2] = 0.0f;
        //quat combined_rot;
        //quat_mul(combined_rot, camera_transform.rotation_quat, mouse_rot);
        //combined_rot[2] = 0.0f;
        //vec4_dup(camera_transform.rotation_quat, combined_rot);
        //printf("quat: [%.3f %.3f %.3f %.3f]\n", mouse_rot[0], mouse_rot[1], mouse_rot[2], mouse_rot[3]);
        //printf("Mouse pos x: %.2f y: %.2f\n", delta_mouse_pos[0], delta_mouse_pos[1]);
        //camera_transform.rot -= delta_mouse_pos[0] * 0.005f;
    }

    vec3_dup(t1.position, pos);
    vec3_dup(t1.rotation, axis);
    vec3_dup(t1.scale, scale);

    pos1[0] = lerp(0.5f, 0.7f, (sin(1.0f * time_elapsed) + 1.0f) * 0.5f);
    pos1[1] = lerp(0.2f, 0.4f, (sin(1.5f * time_elapsed + 0.4f) + 1.0f) * 0.5f);
    vec3 half_scale;
    vec3_scale(half_scale, scale, 0.5f);

    vec3_dup(t2.position, pos1);
    vec3_dup(t2.rotation, axis);
    vec3_dup(t2.scale, half_scale);
    quat spin;
    vec3 euler_angles = { 0.0f, time_elapsed * 0.5f, PI  * 0.25f };
    quat_euler(spin, euler_angles);
    vec4_dup(t2.rotation_quat, spin);

    input_get_cursor_position(prev_mouse_pos, prev_mouse_pos + 1);
}

struct QuadColorData quad_color_data;

void features_init()
{
    quad_color_data.shader = &color_shader;
    quad_color_system_init(&quad_color_data);
}

void features_update(double delta_time, double time_elapsed)
{
    fullscreen_system();
    movement_system(delta_time, time_elapsed);
    quad_color_system_update(&quad_color_data, time_elapsed);
}

void update(double delta_time, double time_elapsed)
{
    features_update(delta_time, time_elapsed);
    
    int width = engine_get_window_width();
    int height = engine_get_window_height();

    CameraData camera = { .transform = camera_transform, .fov = 0.8f, .near_clip = 0.1f, .far_clip = 100.0f, width, height };
    camera_draw(&camera, &opaque_renderers, &transparent_renderers);

    if (input_get_key_down(KEY_O))
    {
        glDrawPixels(1920, 1080, GL_RGB, GL_FLOAT, pixel_buffer);
        glFlush();
    }
}